<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	function __construct(){
		parent::__construct();
	
		if($this->session->userdata('status') != "login"){
			redirect(base_url("login"));
		}
		$this->load->model('m_user'); 
	}
	
	public function index()
	{
		$user_list = $this->m_user->user_list();
		$this->template->load('layout', 'user/index', array('user_list' => $user_list));
	}

	public function delete($id)
	{
		$this->m_user->user_delete($id);
		redirect('user/index');
	}

	public function create()
	{		
		$this->template->load('layout', 'user/create');
	}

	public function create_save()
	{
		$fullname = $this->input->post('fullname');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$param = array(
			'fullname' => $fullname,
			'username' => $username,
			'password' => md5($password)
		);
		$this->m_user->user_create($param);
		redirect('user/index');
	}

	public function edit($id)
	{		
		$user = $this->m_user->user_get($id);
		$this->template->load('layout', 'user/edit', array("user" => $user, "id" => $id));
	}

	public function edit_save()
	{
		$id = $this->input->post('id');
		$fullname = $this->input->post('fullname');
		$password = $this->input->post('password');

		if(isset($password)){
			$param = array(
				'fullname' => $fullname,
				'password' => md5($password)
			);
		}else {
			$param = array(
				'fullname' => $fullname
			);	
		}
		
		$this->m_user->user_update($id, $param);

		redirect('user/index');
	}
}
