<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is used for tracking bike from GPS
 */
class BikeControl extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    public function bike_status_get()
    {
        $track = $this->db->select('*')->limit(1)->get('tbl_bike_status')->row();
        
        if (!empty($track))
        {
            $this->set_response($track, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'Data is empty'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function turn_on_bike_post()
    {        
        $track = $this->db->select('*')->limit(1)->get('tbl_bike_status')->row();
        
        if (!empty($track))
        {
            $this->db->where('id', $track->id);
            $update = $this->db->update("tbl_bike_status", array('status' => 1));    
            if ($update) {
                $this->response(array('status' => 1), 200);
            } else {
                $this->response(array('status' => 'fail', 502));
            }
        }
        else
        {
            $data = array('status' => 1);
            $insert = $this->db->insert('tbl_bike_status', $data);
            if ($insert) {
                $this->response($data, 200);
            } else {
                $this->response(array('status' => 'fail', 502));
            }
        }        
    }

    public function turn_off_bike_post()
    {        
        $track = $this->db->select('*')->limit(1)->get('tbl_bike_status')->row();
        
        if (!empty($track))
        {
            $this->db->where('id', $track->id);
            $update = $this->db->update("tbl_bike_status", array('status' => 0));    
            if ($update) {
                $this->response(array('status' => 0), 200);
            } else {
                $this->response(array('status' => 'fail', 502));
            }
        }
        else
        {
            $data = array('status' => 0);
            $insert = $this->db->insert('tbl_bike_status', $data);
            if ($insert) {
                $this->response($data, 200);
            } else {
                $this->response(array('status' => 'fail', 502));
            }
        }        
    }
}
