<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is used for tracking bike from GPS
 */
class TrackMyBike extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    public function last_poistion_get()
    {
        $track = $this->db->select('*')->order_by('timestamp','desc')->limit(1)->get('tbl_bike_position')->row();
        
        if (!empty($track))
        {
            $this->set_response($track, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'Data is empty'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function tracker_get()
    {
        // tracker from a data store e.g. database
        $id = $this->get('id');
        if ($id == '') {
            $track = $this->db->get('tbl_bike_position')->result();
        } else {
            $this->db->where('id', $id);
            $track = $this->db->get('tbl_bike_position')->result();
        }

        if (!empty($track))
        {
            $this->set_response($track, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'Data is empty'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function tracker_post()
    {
        $now = date('Y-m-d H:i:s');

        $data = array(
            'latitude' => $this->post('latitude'),
            'longitude' => $this->post('longitude'),
            'created_date' => $now
        );
        
        $track = $this->db->select('*')->order_by('timestamp','desc')->limit(1)->get('tbl_bike_position')->row();

        // if position change, then save new record, otherwise do nothing
        if($track->latitude == $data['latitude'] && $track->longitude == $data['longitude']){
            $this->set_response(['status' => TRUE, 'message' => 'Data already exists'], REST_Controller::HTTP_OK);
        }else{
            $insert = $this->db->insert('tbl_bike_position', $data);
            if ($insert) {
                $this->response($data, 200);
            } else {
                $this->response(array('status' => 'fail', 502));
            }
        }        
    }
}
