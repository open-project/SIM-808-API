<?php 
 
class M_user extends CI_Model{	
	
	function user_list(){		
		return $this->db->get('users')->result();
	}

	function user_get($id){		
		$this->db->where("id", $id);
        return $this->db->get("users")->row();
	}

	function user_delete($id){		
		$this->db->where("id", $id);
        $this->db->delete("users");
	}

	function user_update($id, $param){		
		$this->db->where("id", $id);
        $this->db->update("users", $param);
	}

	function user_create($param){
        $this->db->insert('users', $param);
	}
}