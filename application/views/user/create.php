<section class="content-header">
	<h1>
		User
		<small>Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-user"></i> User</a></li>
	</ol>
</section>
<section class="content">    
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Create User</h3>
				<div>
				 <form name="create_user" action="<?php echo base_url('user/create_save')?>" method="POST" role="form">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="nama">
                                Username
                                <span class="required-indicator">*</span>
                            </label>
                            <input type="text" name="username" class="form-control" id="username" required/>
						</div>
						<div class="form-group">
                            <label for="nama">
                                Fullname
                                <span class="required-indicator">*</span>
                            </label>
                            <input type="text" name="fullname" class="form-control" id="fullname" required/>
						</div>
						<div class="form-group">
                            <label for="nama">
                                Password
                                <span class="required-indicator">*</span>
                            </label>
                            <input type="password" name="password" class="form-control" id="password" required/>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="submit" name="create" class="btn btn-success" value="Create"/>
                        <a href="<?php echo base_url('user/index')?>" class="btn btn-default">Back</a>
                    </div>
                </form>
			</div>
        </div>
    </div>
</section>