<section class="content-header">
	<h1>
		User
		<small>Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-user"></i> User</a></li>
	</ol>
</section>
<section class="content">    
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">User List</h3>

					<div class="box-tools pull-right">
						<a href="<?php echo base_url('user/create')?>" class="btn btn-default btn-sm" data-toggle="tooltip" title="Create">
							<i class="fa fa-plus"></i>
						</a>
					</div>
				<div>
				<div class="box-body">
					<table class="table table-bordered table-striped">
						<thead>
							<th>No.</th>
							<th>Username</th>
							<th>Fullname</th>
							<th>Action</th>
						</thead>
						<tbody>
							<?php foreach($user_list as $key => $value){ ?>
								<tr>
									<td><?php echo ($key+1); ?></td>
									<td><?php echo $value->username; ?></td>
									<td><?php echo $value->fullname; ?></td>
									<td>
										<a href="<?php echo base_url("user/delete/".$value->id."")?>" onclick="return confirm('Are you sure want to delete this data?')"><i class="fa fa-trash"></i></a>
										<a href="<?php echo base_url("user/edit/".$value->id."")?>"><i class="fa fa-edit"></i></a>											
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
        </div>
    </div>
</section>